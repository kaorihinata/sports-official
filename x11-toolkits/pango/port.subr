PORTNAME="pango"
PORTVERSION="1.50.14"
DEPENDS="\
../../converters/fribidi
../../devel/glib20
../../devel/gobject-introspection
../../devel/meson
../../devel/ninja
../../devel/pkg-config
../../graphics/cairo
../../print/freetype2
../../print/harfbuzz
../../x11-fonts/fontconfig"


FETCHURL="https://download.gnome.org/sources/pango/${PORTVERSION%.*}"
FETCHURL="$FETCHURL/pango-$PORTVERSION.tar.xz"
VERSIONURL="${FETCHURL%/*/*}/cache.json"
VERSIONREGEX='/pango-\([0-9\.]*\)\.tar\.gz"'

. ../../Share/unixlike.subr

# Note: Dependencies lock us to 1.50.14.
unset -f port_health

port_configure() {
    cd "pango-$PORTVERSION"
    set -- --prefix="$PREFIX" -D"cairo=enabled" -D"default_library=both" \
        -D"fontconfig=enabled" -D"freetype=enabled" 
    if [ -f "$PREFIX/share/gir-1.0/GObject-2.0.gir" ]; then
        set -- "$@" -D"introspection=enabled"
    else
        set -- "$@" -D"introspection=disabled"
    fi
    meson setup _build "$@"
}

port_build() {
    cd "pango-$PORTVERSION"
    meson compile -C _build
}

port_install() {
    cd "pango-$PORTVERSION"
    meson install -C _build
}

port_test() {
    cd "pango-$PORTVERSION"
    meson test -C _build
}
